import argparse
import re
from DatabaseFunctions import *
import random


def arguement_parser():
    parser = argparse.ArgumentParser(description='Create a socket server')
    parser.add_argument('-b', "--bootstrap-ip", type=str, help='Bootstrap server IP address', required=True)
    parser.add_argument('-p', "--port", type=int, help='Port Number to use for the server', required=True)
    parser.add_argument('-n', "--bootstrap-port", type=int, help='Bootstrap server port number', required=True)
    parser.add_argument('-u', "--username", type=str, help='Username to connect to bootstrap')
    args = parser.parse_args()
    return args


def read_resource_txt():
    data = open('resources.txt', 'r').read().replace('\r', '').split('\n')
    search_list = []
    count = 1
    for index in range(len(data)):
        if len(data[index]) > 0 and data[index][0] != '#':
            search_list.append(data[index])

    return search_list


def select_random_files(file_list,length):
    selected_files = []
    for i in range(length):
        selected_files.append(file_list.pop(int(random.random() * 1000) % len(file_list)))

    for i in range(len(selected_files)):
        FileTable.create(filename=selected_files[i], index=i,file_exists_on_node_flag=True)

    return selected_files

