import logging
import re


class PeerProtocol:
    def __init__(self):
        pass

    def _string_len_prepend(self, string):
        return '%s %s' % (str(len(string)).zfill(3), string)

    def leave_request(self, ip, port):
        return self._string_len_prepend('LEAVE %s %s' % (ip, port))

    def leave_response(self, error_code):
        return self._string_len_prepend('LEAVEOK %d' % (error_code))

    def join_request(self, ip, port):
        return self._string_len_prepend('JOIN %s %s' % (ip, port))

    def join_response(self, error_code):
        return self._string_len_prepend('JOINOK %d' % (error_code))

    def search_request(self, ip, port, filename, hops):
        return self._string_len_prepend('SER %s %d "%s" %d' % (ip, port, filename, hops))

    def search_response(self, ip, port, error_code, filename=None, hops=0):
        if filename is not None and len(filename) > 0 and error_code >= 0:
            file_concat = ""
            error_code = len(filename)
            for filex in filename:
                file_concat += '"%s"' % filex
            return self._string_len_prepend('SEROK %s %d %d %d %s' % (ip, port, error_code, hops, file_concat))
        else:
            return self._string_len_prepend('SEROK %s %d %d' % (ip, port, error_code))

    def unknown_request(self, error_code):
        return self._string_len_prepend('UNKNOWN %d' % (error_code))

    def parse_request(self, data):
        regex_string = r'(JOIN|LEAVE|SER|[A-Z]+)(' \
                       r'(?<=JOIN) (?P<address_join>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+\d+)|' \
                       r'(?<=LEAVE) (?P<address_leave>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+\d+)|' \
                       r'(?<=SER) (?P<address_search>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+) "(?P<search_string>.*)" (?P<hops>\d+)|' \
                       r'(?<=SER)(?P<ser_resp>OK) (?P<node_address>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+) (?P<error_code_search>\d+)( (?P<hops_response>\d+)(?P<filename>( ".*?"){0,}))?|' \
                       r' (?P<unspecified_data>.*)' \
                       r')'

        response_validate = re.search(regex_string, data)
        # print response_validate.groups()
        if response_validate is None:
            logging.error('Invalid Server Request.')
            return
        request_type = response_validate.group(1)
        request = None

        if request_type == 'JOIN':
            request = {}
            request['type'] = request_type
            request['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                            response_validate.group('address_join') if response_validate.group(
                                                'address_join') is not None else '')
        elif request_type == 'LEAVE':
            request = {}
            request['type'] = request_type
            request['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                            response_validate.group('address_leave') if response_validate.group(
                                                'address_leave') is not None else '')
        elif request_type == 'SER':
            request = {}
            request['type'] = request_type
            if response_validate.group('ser_resp') == 'OK':
                request['response_flag'] = True
                request['error_code'] = int(response_validate.group('error_code_search'))
                if response_validate.group('hops_response'):
                    request['hops'] = int(response_validate.group('hops_response'))
                request['source_address'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                                       response_validate.group(
                                                           'node_address') if response_validate.group(
                                                           'node_address') is not None else '')
                if response_validate.group('filename') is not None:
                    request['filename'] = re.findall(r'"(.*?)"',response_validate.group('filename'))

            else:
                request['response_flag'] = False
                request['source_address'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                                       response_validate.group(
                                                           'address_search') if response_validate.group(
                                                           'address_search') is not None else '')
                request['search_string'] = response_validate.group('search_string')
                request['hops'] = int(response_validate.group('hops'))

        else:
            request = {}
            request['type'] = request_type
            request['data'] = response_validate.group('unspecified_data')

        # Format IP and Port
        if request.get('clients') is not None:
            clients = []
            for ip, port in request['clients']:
                clients += [(ip, int(port))]
            request['clients'] = clients

        if request.get('source_address') is not None:
            clients = []
            for addr in request['source_address']:
                ip, port = addr
                clients += [(ip, int(port))]
            request['source_address'] = clients

        return request

    def parse_response(self, data):
        regex_string = r'(JOIN|LEAVE|SER)(' \
                       r'(?<=JOIN)OK (?P<error_code_join>-?\d+)|' \
                       r'(?<=LEAVE)OK (?P<error_code_leave>-?\d+)|' \
                       r'(?<=SER)(?P<ser_resp>OK) (?P<node_address>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+) (?P<error_code_search>\d+)( (?P<hops_response>\d+)(?P<filename>( ".*?"){0,}))?|' \
                       r')'

        response_validate = re.search(regex_string, data)
        if response_validate is None:
            logging.error('Invalid Client Response.Data Received:%s' % data)
            return

        response_type = response_validate.group(1)
        response = {}

        if response_type == 'JOIN':
            response['type'] = response_type
            response['error_code'] = int(response_validate.group('error_code_join'))
        elif response_type == 'LEAVE':
            response['type'] = response_type
            response['error_code'] = int(response_validate.group('error_code_leave'))
        elif response_type == 'SER':
            response['type'] = response_type
            response['error_code'] = int(response_validate.group('error_code_search'))
            response['hops'] = int(response_validate.group('hops_response'))
            response['filename'] = response_validate.group('filename')

        return response
