import socket
import re


class BSError(ValueError):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args)


class BSRegistrationError(BSError):
    UNKNOWN = -1
    ERROR = 9999
    REGISTERED = 9998

    def __init__(self, *args, **kwargs):
        BSError.__init__(self, *args, **kwargs)
        self.errno = kwargs['error_code']


class BSDeleteError(BSError):
    def __init__(self, *args, **kwargs):
        BSError.__init__(self, *args, **kwargs)
        self.errno = kwargs['error_code']


class BSGetError(BSError):
    def __init__(self, *args, **kwargs):
        BSError.__init__(self, *args, **kwargs)
        self.errno = kwargs['error_code']


class BootstrapServer:
    def __init__(self, hostname=None, port=0, username=None, ip_list_file=None):
        # Read the bootstrap server list file
        if ip_list_file is not None:
            self.server_list = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)', open(ip_list_file).read(),
                                          re.MULTILINE)

        # Init Hostname
        self._server_address_set_flag = False if hostname is None and port is 0 else True
        self._username_set_flag = False if username is None else True

        # Set Socket and Timeout
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.sock.settimeout(5)

        # Update the dict variables
        self.__dict__.update(dict(zip(('hostname', 'port', 'username'), (hostname, int(port), username))))

    def _get_server(self, server_index):
        if server_index < len(self.server_list):
            return dict(zip(('hostname', 'port'), self.server_list[server_index]))
        else:
            raise BSError('Invalid Server Index')

    def _socket_receive(self):
        data = self.sock.recv(1000)
        data_len, data = re.findall(r'(\d{4}) (.*)', data)[0]
        if int(data_len) == len(data):
            return data
        else:
            print 'Received Response Data Length Mismatch'
            return data

    def _handle_error_response(self, response):
        errcode = response['error_code']
        if response['type'] == 'REG':
            if errcode == -1:
                raise BSRegistrationError('Unknown REG command', error_code=errcode)
            elif errcode == 9999:
                raise BSRegistrationError('Error in registering', error_code=errcode)
            elif errcode == 9998:
                raise BSRegistrationError('Already registered with Bootstrapper', error_code=errcode)

        elif response['type'] == 'DEL':
            if errcode == -1:
                raise BSDeleteError('Error in DEL command', error_code=errcode)
            elif errcode == 9998:
                raise BSDeleteError('(IPAddress + Port) not registered for username', error_code=errcode)
            elif errcode == 9999:
                raise BSDeleteError('username not registered with bootstrapper', error_code=errcode)

        elif response['type'] == 'BS':
            if errcode == -9999:
                raise BSError('Unknown command, undefined characters to Bootstrapper.', error_code=errcode)

        elif response['type'] == 'GET':
            if errcode == 9999:
                response['error_code'] = 0
                raise BSGetError('No Clients returned from server', error_code=errcode)
        return response

    def get_address(self):
        return self.hostname, self.port

    def _get_response(self, data):
        """
        1. BS REQ -9999: Unknown command, undefined characters to Bootstrapper.
        2. REG OK <username> -1: Unknown REG command
        3. REG OK 9999: Error in registering
        4. REG OK 9998: Already registered with Bootstrapper
        5. DEL IPADDRESS OK -1 / DEL UNAME OK -1 / DEL OK -1: Error in DEL command
        6. DEL IPADDRESS OK 9998: (IPAddress + Port) not registered for username
        7. DEL UNAME OK 9999: username not registered with bootstrapper


        Resulting Regex string to satisfy all the conditions
        regex_string = r'(REG|DEL|BS|GET)(' \
                       r'(?<=REG)OK ((?P<username_reg>[^0-9_][A-Z_0-9]+) )?(?P<error_code_reg>-?\d+)(?P<client_list>( \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3})|' \
                       r'(?<=DEL) (IPADDRESS|UNAME)? *OK *(?P<username_del>[^0-9_][A-Z_0-9]+)? *(?P<client_list_del>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3}) *(?P<error_code_del>-?\d+)|' \
                       r'(?<=BS) REQ (?P<error_code_bs>\d+)|' \
                       r'(?<=GET) IPLIST OK (?P<username_get>[^0-9_][A-Z_0-9]+) (?P<client_count>\d+) *(?P<client_list_get>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+)+)?' \
                       r')'
        """

        regex_string = r'(REG|DEL|BS|GET)(' \
                       r'(?<=REG)OK ((?P<username_reg>[^0-9_][A-Z_0-9]+) )?(?P<error_code_reg>-?\d+)(?P<client_list>( \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3})|' \
                       r'(?<=DEL) (IPADDRESS|UNAME)? *OK *(?P<username_del>[^0-9_][A-Z_0-9]+)? *(?P<client_list_del>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3}) *(?P<error_code_del>-?\d+)|' \
                       r'(?<=BS) REQ (?P<error_code_bs>\d+)|' \
                       r'(?<=GET) IPLIST OK (?P<username_get>[^0-9_][A-Z_0-9]+) (?P<client_count>\d+) *(?P<client_list_get>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+)+)?' \
                       r')'

        response_validate = re.search(regex_string, data)
        if response_validate is None:
            raise BSError('Response handler failed to handle response.')

        response_type = response_validate.group(1)
        response = {}

        if response_type == 'GET':
            # print response_validate.group('client_list_get')
            response['type'] = response_type
            response['username'] = response_validate.group('username_get')
            response['error_code'] = int(response_validate.group('client_count'))
            response['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                             response_validate.group('client_list_get') if response_validate.group(
                                                 'client_list_get') is not None else '')
        elif response_type == 'REG':
            response['type'] = response_type
            response['username'] = response_validate.group('username_reg')
            response['error_code'] = int(response_validate.group('error_code_reg'))
            response['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                             response_validate.group('client_list') if response_validate.group(
                                                 'client_list') is not None else '')
        elif response_type == 'DEL':
            response['type'] = response_type
            # print response_validate.groups()
            response['username'] = response_validate.group('username_del')
            response['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                             response_validate.group('client_list_del') if response_validate.group(
                                                 'client_list_del') is not None else '')
            response['error_code'] = int(response_validate.group('error_code_del'))
        elif response_type == 'BS':
            response['type'] = response_type
            response['error_code'] = int(response_validate.group('error_code_bs'))

        response = self._handle_error_response(response)

        if response.get('clients') is not None:
            clients = []
            for ip, port in response['clients']:
                clients += [(ip, int(port))]
            response['clients'] = clients

        return response

    def set_username(self, username):
        self.username = username
        self._username_set_flag = True

    def set_server(self, server_id):
        server = self._get_server(server_id)
        if server is not None:
            self.hostname = server['hostname']
            self.port = int(server['port'])
            self._server_address_set_flag = True
        else:
            raise ValueError('Could not find Server at index')

    def _string_len_prepend(self, string):
        return '%s %s' % (str(len(string)).zfill(3), string)

    def register(self, ip_address, port):
        if self._server_address_set_flag is False:
            raise BSError('Server Address Not Set')
        elif self._username_set_flag is False:
            raise BSError('Username is Not set')
        reg_message = 'REG %s %d %s' % (ip_address, port, self.username)
        reg_message = self._string_len_prepend(reg_message)
        self.sock.sendto(reg_message, (self.hostname, self.port))
        return self._get_response(self._socket_receive())

    def deregister_ip(self, ip_address, port):
        if self._server_address_set_flag is False:
            raise BSError('Server Address Not Set')
        elif self._username_set_flag is False:
            raise BSError('Username is Not set')
        message = 'DEL IPADDRESS %s %d %s' % (ip_address, port, self.username)
        message = self._string_len_prepend(message)
        self.sock.sendto(message, (self.hostname, self.port))
        return self._get_response(self._socket_receive())

    def deregister_all(self):
        if self._server_address_set_flag is False:
            raise BSError('Server Address Not Set')
        elif self._username_set_flag is False:
            raise BSError('Username is Not set')
        message = 'DEL UNAME %s' % (self.username)
        message = self._string_len_prepend(message)
        self.sock.sendto(message, (self.hostname, self.port))
        return self._get_response(self._socket_receive())

    def list_all(self):
        if self._server_address_set_flag is False:
            raise BSError('Server Address Not Set')
        elif self._username_set_flag is False:
            raise BSError('Username is Not set')

        message = 'GET IPLIST %s' % (self.username)
        message = self._string_len_prepend(message)
        self.sock.sendto(message, (self.hostname, self.port))
        return self._get_response(self._socket_receive())