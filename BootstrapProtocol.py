import re


class BootstrapProtocol:
    username = None
    ip_address = None
    port = None

    def __init__(self, username):
        self.username = username

    def parse_response(self, data):
        """
        1. BS REQ -9999: Unknown command, undefined characters to Bootstrapper.
        2. REG OK <username> -1: Unknown REG command
        3. REG OK 9999: Error in registering
        4. REG OK 9998: Already registered with Bootstrapper
        5. DEL IPADDRESS OK -1 / DEL UNAME OK -1 / DEL OK -1: Error in DEL command
        6. DEL IPADDRESS OK 9998: (IPAddress + Port) not registered for username
        7. DEL UNAME OK 9999: username not registered with bootstrapper

        Resulting Regex string to satisfy all the conditions
        regex_string = r'(REG|DEL|BS|GET)(' \
                       r'(?<=REG)OK ((?P<username_reg>[^0-9_][A-Z_0-9]+) )?(?P<error_code_reg>-?\d+)(?P<client_list>( \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3})|' \
                       r'(?<=DEL) (IPADDRESS|UNAME)? *OK *(?P<username_del>[^0-9_][A-Z_0-9]+)? *(?P<client_list_del>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3}) *(?P<error_code_del>-?\d+)|' \
                       r'(?<=BS) REQ (?P<error_code_bs>\d+)|' \
                       r'(?<=GET) IPLIST OK (?P<username_get>[^0-9_][A-Z_0-9]+) (?P<client_count>\d+) *(?P<client_list_get>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+)+)?' \
                       r')'
        """

        regex_string = r'(REG|DEL|BS|GET)(' \
                       r'(?<=REG)OK ((?P<username_reg>[^0-9_][A-Za-z_0-9]+) )?(?P<error_code_reg>-?\d+)(?P<client_list>( \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3})|' \
                       r'(?<=DEL) (IPADDRESS|UNAME)? *OK *(?P<username_del>[^0-9_][A-Za-z_0-9]+)? *(?P<client_list_del>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+){0,3}) *(?P<error_code_del>-?\d+)|' \
                       r'(?<=BS) REQ (?P<error_code_bs>\d+)|' \
                       r'(?<=GET) IPLIST OK (?P<username_get>[^0-9_][A-Z_0-9]+) (?P<client_count>\d+) *(?P<client_list_get>( *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} \d+)+)?' \
                       r')'

        response_validate = re.search(regex_string, data)
        if response_validate is None:
            return None

        response_type = response_validate.group(1)
        response = {}

        if response_type == 'GET':
            response['type'] = response_type
            response['username'] = response_validate.group('username_get')
            response['error_code'] = int(response_validate.group('client_count'))
            response['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                             response_validate.group('client_list_get') if response_validate.group(
                                                 'client_list_get') is not None else '')
        elif response_type == 'REG':
            response['type'] = response_type
            response['username'] = response_validate.group('username_reg')
            response['error_code'] = int(response_validate.group('error_code_reg'))
            response['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                             response_validate.group('client_list') if response_validate.group(
                                                 'client_list') is not None else '')
        elif response_type == 'DEL':
            response['type'] = response_type
            response['username'] = response_validate.group('username_del')
            response['error_code'] = int(response_validate.group('error_code_del'))
            response['clients'] = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\d+)',
                                             response_validate.group('client_list_del') if response_validate.group(
                                                 'client_list_del') is not None else '')

        elif response_type == 'BS':
            response['type'] = response_type
            response['error_code'] = int(response_validate.group('error_code_bs'))

        # Format IP and Port
        if response.get('clients') is not None:
            clients = []
            for ip, port in response['clients']:
                clients += [(ip, int(port))]
            response['clients'] = clients

        return response

    def register_request(self, ip_address, port):
        message = 'REG %s %d %s' % (ip_address, port, self.username)
        message = self._string_len_prepend(message)
        return message

    def deregister_ip_request(self, ip_address, port):
        message = 'DEL IPADDRESS %s %d %s' % (ip_address, port, self.username)
        message = self._string_len_prepend(message)
        return message

    def deregister_all(self, username):
        message = 'DEL UNAME %s' % (username)
        message = self._string_len_prepend(message)
        return message

    def list_all(self):
        message = 'GET IPLIST %s' % (self.username)
        message = self._string_len_prepend(message)
        return message

    def _string_len_prepend(self, string):
        return '%s %s' % (str(len(string)).zfill(3), string)
