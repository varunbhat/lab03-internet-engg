from peewee import *
import os
import socket
import time

db = SqliteDatabase('/tmp/%d_%s.db' % (os.getpid(), socket.gethostname()))
# db = SqliteDatabase('/tmp/%s.db' % (socket.gethostname()))


class IncomingClients(Model):
    ip = CharField()
    port = IntegerField()
    registration_time = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])
    responded_flag = BooleanField(default=False)

    class Meta:
        database = db
        constraints = [SQL('UNIQUE (ip, port) ON CONFLICT REPLACE')]


class OutgoingClients(Model):
    ip = CharField()
    port = IntegerField()
    # registration_time = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])
    responded_flag = BooleanField(default=False)

    class Meta:
        database = db
        constraints = [SQL(' UNIQUE (ip, port) ON CONFLICT REPLACE')]


class FileTable(Model):
    index = IntegerField()
    filename = CharField()
    file_exists_on_node_flag = BooleanField(default=False)

    class Meta:
        database = db
        constraints = [SQL('UNIQUE (filename) ON CONFLICT REPLACE')]


class SearchResults(Model):
    search_results = CharField()
    destination_ip = CharField()
    destination_port = IntegerField()
    hop_count = IntegerField()
    response_time = FloatField(default=time.time())
    # response_time = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])

    class Meta:
        database = db


class SearchRequests(Model):
    search_string = CharField()
    source_ip = CharField()
    source_port = IntegerField()
    hop_count = IntegerField()
    # request_time = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])
    request_time = DateTimeField(default=time.time())
    ignore_duplicate_queries_flag = BooleanField(default=False)
    forwarded_node_count = IntegerField()

    class Meta:
        database = db
