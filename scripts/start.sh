#!/bin/bash

hostname
cd ~/.varun/KilenjeNataraj_VarunBhat_Lab03/
#cd /home/varunbhat/PycharmProjects/Lab03/KilenjeNataraj_VarunBhat_Lab03
virtualenv --distribute .env
. .env/bin/activate

pip install -r requirements.txt
pwd

# python clear_bootstrap.py
hostname && ps -ef |grep python|grep varun|grep -v hostname | awk {print\$2} | xargs kill -9
setsid python $PWD/main.py -p 10223 -b 129.82.46.207 -n10000 >/dev/null 2>&1 < /dev/null &
